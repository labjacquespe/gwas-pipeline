# GWAS pipeline

Simple GWAS pipeline using SNPTEST. 
The pipeline is held in a bash script that performs data formating for SNPTEST, run SNPTEST, filters output files and annotates SNPs following GenDiP2 criteria.

### Usage

1) Output path: set output directory **OUTDIR** and the name of the **DATASET** tested (will be used as subdirectory).
```bash
OUTDIR="gwas"
DATASET="mothers"

mkdir -p $OUTDIR
OUTPATH=${OUTDIR}/${DATASET}
mkdir -p $OUTPATH
```

2) Genotypes: Indicate genotype file path, must be a **VCF**. Indicate annotation file which is a tab delimitated file with 5 columns (SNP ID, imputation score, imputation status, call rate, HWE p-value).
```bash
VCF_FILE=/path/to/vcf/file.vcf.gz
SNP_ANNOTATION=/path/to/SNP/annotation/file.tsv
```

3) Included samples: Provide a list of sample to include in analyzes
```bash
INCLUDED_SAMPLES=${OUTPATH}/sample_list
#bcftools query -l $VCF_FILE > $INCLUDED_SAMPLES
```

4) Phenotypes: Indicate the file containing all phenotypes.
```bash
PHENOTYPE_FILE=/path/to/all/phenotypes.tsv
```

5) Sample file: Prepare SNPTEST **sample file** by creating a template with phenotypes and covariables to be included in the analysis. First column is the type of variable used by SNPTEST, second column is a choosen variable name and in the third column must be the name of the variables as they appear in $PHENOTYPE_FILE.

```bash
SAMPLE_FILE_TEMPLATE=${OUTPATH}/template_sample_file

cat << END > $SAMPLE_FILE_TEMPLATE
0 sample_id ID
D sex mother_sex
C PC1 mother_PC1
C PC2 mother_PC2
C PC3 mother_PC3
C PC4 mother_PC4
B GDM Visite_Diagnostic_de_DG_V2
P FastingGlucose Visite_HGOP_0_min_V2
P OGTT1H Visite_HGOP_60_min_V2
P OGTT2H Visite_HGOP_120_min_V2
P HbA1c Visite_HbA1c_BC0_V2
P Matsuda MATSUDA
END
```

6) Run GWAS with phenotype of interest
```bash
bash pipeline_gwas_snptest.sh GDM $VCF_FILE $PHENOTYPE_FILE $DATASET $OUTPATH $SNP_ANNOTATION
```