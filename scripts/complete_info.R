
args = commandArgs(trailingOnly=TRUE)
snptest_results = args[1]
pheno = args[2]
dataset=args[3]
annotation_file=args[4]
final_output=args[5]

if (pheno == "GDM") {
	final_table_colnames = c("SNPID","STRAND","BUILD","CHR","POS","EFFECT_ALLELE","NON_EFFECT_ALLELE","N_CASES","N_CONTROLS","N0","N1","N2","EAF","HWE_P","CALL_RATE","BETA","SE","PVAL","IMPUTED","INFO_TYPE","INFO")
} else {
	final_table_colnames = c("SNPID","STRAND","BUILD","CHR","POS","EFFECT_ALLELE","NON_EFFECT_ALLELE","N","N0","N1","N2","EAF","HWE_P","CALL_RATE","BETA","SE","PVAL","IMPUTED","INFO_TYPE","INFO")
}

initial_table_colnames = c("ID_for_merge", final_table_colnames)
print(snptest_results)
print(pheno)
gwas = read.csv(snptest_results, sep=" ", header=T)
print(dim(gwas))
colnames(gwas) = initial_table_colnames
gwas$CALL_RATE = gwas$IMPUTED = gwas$INFO = gwas$HWE_P = NULL

# Add HWE_P and INFO
stats = read.csv(annotation_file, sep="\t", header=F)
colnames(stats) = c("ID_for_merge","INFO","IMPUTED","CALL_RATE","HWE_P")

#print(gwas[1:5,1:5])
#print(head(stats))
gwas = merge(gwas, stats, by="ID_for_merge")
print(dim(gwas))

# CALL_RATE 
# Genotyping call rate for the SNP (N_successful/N_attempted) 
# Frequency 4 digits to the right of the decimal. 
# !! Set to 1.000 if imputed 
gwas$CALL_RATE = ifelse(gwas$CALL_RATE==1, "1.000", gwas$CALL_RATE )
# HWE_P
# Exact HWE p-value for samples analysed, only if genotyped data is used in analysis 
# Scientific E notation with 4 digits to the right of the decimal (set to missing for imputed SNPs) 
gwas$HWE_P = ifelse(gwas$IMPUTED==1,".", gsub("e", "E",format(gwas$HWE_P, scientific=T, digits=4)))

# INFO_TYPE 
# Type of information provided in the INFO column 
# Text, (set to missing if genotyped) 
if(dataset == "mothers"){
	gwas$INFO_TYPE = ifelse(gwas$IMPUTED==1,"R2", ".")
} else {
	gwas$INFO_TYPE = ifelse(gwas$IMPUTED==1,"IMPUTE", ".")
}

# INFO 
# Measure of information content for the imputed SNP result 
# Numeric float with 4 digits to the right of the decimal (set to missing if genotyped) 
gwas$INFO = ifelse(gwas$IMPUTED==1, gwas$INFO, ".")

gwas = gwas[, final_table_colnames]
print(dim(gwas))

write.table(gwas, file=final_output, sep="\t", row.names=F, quote=F)

date_str = format(Sys.time(), "%Y%m%d")

library(qqman)
png(paste0("plot/manhattan_GenDiP_TOPMed.",pheno,".",dataset,".Gen3G.european.FW.",date_str,".png"))
sub_gwas = gwas 
sub_gwas$CHR = as.numeric(as.factor(sub_gwas$CHR))
sub_gwas$POS = as.numeric(sub_gwas$POS)
sub_gwas$PVAL = as.numeric(sub_gwas$PVAL)
#save(sub_gwas, file="mom_signif_GDM.RData")
manhattan(sub_gwas, chr="CHR", bp="POS", snp="SNPID", p="PVAL")
dev.off()

png(paste0("plot/qq_GenDiP_TOPMed.",pheno,".",dataset,".Gen3G.european.FW.",date_str,".png"))
qq(gwas$PVAL)
dev.off()

#### lambda # https://stats.stackexchange.com/questions/110755/calculate-inflation-observed-and-expected-p-values-from-uniform-distribution-in
chisq = qchisq(1-as.numeric(gwas$PVAL), 1)
lambda = median(chisq)/qchisq(0.5,1)
print(lambda)

# If your data follows the normal chi-squared distribution (no inflation), 
#  the expected λ value is 1. 
# If the λ value is greater than 1, 
#  then this may be evidence for some systematic bias that needs to be corrected in your analysis.


