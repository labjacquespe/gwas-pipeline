#!/bin/bash

INFILE=$1

PHENO=$(head -12 ${INFILE} | grep "pheno" | awk '{print $3}')
DIR=$(dirname ${INFILE})

echo $INFILE
echo $DIR
echo $PHENO

### For GDM
if [ "$PHENO" = "GDM" ]; then
    echo "dichotomic"

    awk ' BEGIN { 
        OFS="\t"; print "SNPID","STRAND","BUILD","CHR","POS","EFFECT_ALLELE","NON_EFFECT_ALLELE","N_CASES","N_CONTROLS","N0","N1","N2","EAF","HWE_P","CALL_RATE","BETA","SE","PVAL","IMPUTED","INFO_TYPE","INFO"
     } 
     $1!="alternate_ids" && NF==46 && $42!="NA" {
        OFS="\t";
        EAF=sprintf("%.4f",$29);
        beta=sprintf("%.4f",$44);
        se=sprintf("%.4f",$45);
        pval=sprintf("%.3E",$42);
        print $2,".","38",$3,$4,$6,$5,$19+$20+$21,$24+$25+$26,$14,$15,$16,EAF,".",".",beta,se,pval,".",".","."
     }' \
     $INFILE | \
     tee ${DIR}/GenDiP.${PHENO}.Gen3G.european.FW.20230830.txt | \
     awk '{print $4":"$5":"$7":"$6"\t"$0} ' \
      > ${DIR}/${PHENO}_associations.tsv


else
    ### For continuous phenotypes
    echo "continuous"

    awk ' BEGIN { 
        OFS="\t"; print "SNPID","STRAND","BUILD","CHR","POS","EFFECT_ALLELE","NON_EFFECT_ALLELE","N","N0","N1","N2","EAF","HWE_P","CALL_RATE","BETA","SE","PVAL","IMPUTED","INFO_TYPE","INFO"
     } 
     $1!="alternate_ids" && NF==25 && $21!="NA" {
        OFS="\t"; 
        EAF=sprintf("%.4f",$19);
        beta=sprintf("%.4f",$23);
        se=sprintf("%.4f",$24);
        pval=sprintf("%.3E",$21);
        print $2,".","38",$3,$4,$6,$5,$18-$17,$14,$15,$16,EAF,".",".",beta,se,pval,".",".","."
     }' \
    $INFILE | \
     tee ${DIR}/GenDiP.${PHENO}.Gen3G.european.FW.20230830.txt | \
     awk '{print $4":"$5":"$7":"$6"\t"$0} ' \
     > ${DIR}/${PHENO}_associations.tsv

fi






