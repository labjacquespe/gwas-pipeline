#!/bin/bash

ml r/4.3.1
# ml bcftools/1.16
PHENO=$1
INPUT_GENOTYPES=$2
PHENOTYPE_DB=$3
DATASET=$4
OUTDIR=$5
SNP_ANNOTATION=$6

mkdir -p ${OUTDIR}/scripts
mkdir -p ${OUTDIR}/snptest
mkdir -p ${OUTDIR}/logs
mkdir -p ${OUTDIR}/GenDiP

TIME=$(date +20%y%m%d_%Hh%Mm%Ss)
JOB_LIST=${OUTDIR}/${PHENO}_job_list_${TIME}.txt

echo "Init $PHENO GWAS for $DATASET" > $JOB_LIST
echo "Using VCF: $INPUT_GENOTYPES" >> $JOB_LIST
echo "Using phenotype source: $PHENOTYPE_DB" >> $JOB_LIST

echo -e "JOB_ID\tJOB_NAME\tDEPENDENCIES" >> $JOB_LIST
#-------------------------------------------------------------------------------
# JOB: 1_create_sample_file
#-------------------------------------------------------------------------------
JOB_NAME=create_sample_file_${PHENO}
SAMPLE_FILE=${OUTDIR}/${PHENO}.sample_file
COMMAND=${OUTDIR}/scripts/1_${JOB_NAME}.sh
cat << END > $COMMAND
Rscript scripts/create_sample_file.R $PHENO $PHENOTYPE_DB $SAMPLE_FILE $DATASET $OUTDIR
END

if [ -f $SAMPLE_FILE ]; then
   JOBID_create_sample_file=1
   echo -e "DONE\t$JOB_NAME\t-" >> $JOB_LIST
else
   JOBID_create_sample_file=$(echo "#! /bin/bash" $COMMAND | sbatch --mail-type=FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D "./" -o ${OUTDIR}/logs/${JOB_NAME}.%j.o -J $JOB_NAME --time=0-0:5 --mem-per-cpu 100M -c 1  | grep "[0-9]" | cut -d\  -f4)
   echo -e "$JOBID_create_sample_file\t$JOB_NAME\t-" >> $JOB_LIST
fi

#-------------------------------------------------------------------------------
# JOB: 2_snptest
#-------------------------------------------------------------------------------
JOB_NAME=snptest_${PHENO}
SNPTEST_OUTPUT=${OUTDIR}/snptest/gwas.${PHENO}.tsv
COMMAND=${OUTDIR}/scripts/2_${JOB_NAME}.sh
cat << END > $COMMAND
~/snptest_v2.5.6_CentOS_Linux7.8.2003-x86_64_dynamic/snptest_v2.5.6 \
-data $INPUT_GENOTYPES $SAMPLE_FILE \
-genotype_field GT \
-frequentist 1 \
-method score \
-chunk 1000000 \
-o $SNPTEST_OUTPUT \
-include_samples ${OUTDIR}/${PHENO}.included_samples \
-pheno $PHENO
END

if [ -f $SNPTEST_OUTPUT ]; then
	JOBID_snptest=2
	echo -e "DONE\t$JOB_NAME\t-" >> $JOB_LIST
else
   JOBID_snptest=$(echo "#! /bin/bash" $COMMAND | sbatch --mail-type=FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D "./" -o ${OUTDIR}/logs/${JOB_NAME}.%j.o -J $JOB_NAME --time=1-0 --mem-per-cpu 20G -c 1 --depend=afterok:$JOBID_create_sample_file | grep "[0-9]" | cut -d\  -f4)
   echo -e "$JOBID_snptest\t$JOB_NAME\t$JOBID_create_sample_file" >> $JOB_LIST
fi

#-------------------------------------------------------------------------------
# JOB: 3_filter_gwas_output
#-------------------------------------------------------------------------------
JOB_NAME=filter_gwas_output_${PHENO}
FILTERED_OUTPUT=${OUTDIR}/snptest/formated.${PHENO}.tsv
COMMAND=${OUTDIR}/scripts/3_${JOB_NAME}_${PHENO}.sh

PHENO_TYPE=$(grep $PHENO $OUTDIR/template_sample_file | cut -f1 -d \ )

#### option for continuous phenotypes
COMMAND_BINARY=${OUTDIR}/scripts/tmp_3_b
cat << END > $COMMAND_BINARY
echo "UID SNPID STRAND BUILD CHR POS EFFECT_ALLELE NON_EFFECT_ALLELE N_CASES N_CONTROLS N0 N1 N2 EAF HWE_P CALL_RATE BETA SE PVAL IMPUTED INFO_TYPE INFO" > $FILTERED_OUTPUT && \
awk '  
 \$1!="alternate_ids" && NF==46 && \$42!="NA" {
    OFS=" ";
    EAF=sprintf("%.4f",\$29);
    beta=sprintf("%.4f",\$44);
    se=sprintf("%.4f",\$45);
    pval=sprintf("%.3E",\$42);
    print \$3":"\$4":"\$5":"\$6,\$2,".","38",\$3,\$4,\$6,\$5,\$19+\$20+\$21,\$24+\$25+\$26,\$14,\$15,\$16,EAF,".",".",beta,se,pval,".",".","."
 }' \
 $SNPTEST_OUTPUT >> $FILTERED_OUTPUT
END

#### option for continuous phenotypes
COMMAND_CONTINUOUS=${OUTDIR}/scripts/tmp_3_c
cat << END > $COMMAND_CONTINUOUS
echo "UID SNPID STRAND BUILD CHR POS EFFECT_ALLELE NON_EFFECT_ALLELE N N0 N1 N2 EAF HWE_P CALL_RATE BETA SE PVAL IMPUTED INFO_TYPE INFO" > $FILTERED_OUTPUT && \
awk '  
 \$1!="alternate_ids" && NF==25 && \$21!="NA" {
    OFS=" "; 
    EAF=sprintf("%.4f",\$19);
    beta=sprintf("%.4f",\$23);
    se=sprintf("%.4f",\$24);
    pval=sprintf("%.3E",\$21);
    print \$3":"\$4":"\$5":"\$6,\$2,".","38",\$3,\$4,\$6,\$5,\$18-\$17,\$14,\$15,\$16,EAF,".",".",beta,se,pval,".",".","."
 }' \
$SNPTEST_OUTPUT >> $FILTERED_OUTPUT
END


if [ "$PHENO_TYPE" = "B" ]; then
	mv $COMMAND_BINARY $COMMAND
	rm $COMMAND_CONTINUOUS
else
	mv $COMMAND_CONTINUOUS $COMMAND
	rm $COMMAND_BINARY 
fi

if [ -f $FILTERED_OUTPUT ]; then 
   JOBID_filter=3
   echo -e "DONE\t$JOB_NAME\t-" >> $JOB_LIST
else
   JOBID_filter=$(echo "#! /bin/bash" $COMMAND | sbatch --mail-type=FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D "./" -o ${OUTDIR}/logs/${JOB_NAME}.%j.o -J $JOB_NAME --time=0-0:15 --mem-per-cpu 2G -c 1 --depend=afterok:$JOBID_snptest | grep "[0-9]" | cut -d\  -f4)
   echo -e "$JOBID_filter\t$JOB_NAME\t$JOBID_snptest" >> $JOB_LIST
fi

#-------------------------------------------------------------------------------
# JOB: 4_complete_info
#-------------------------------------------------------------------------------
JOB_NAME=complete_info_${PHENO}
COMMAND=${OUTDIR}/scripts/4_${JOB_NAME}.sh

if [ "$DATASET" = "mothers" ]; then
   PANEL=TopMed
   SAMPLE_SET=mother
else
   PANEL=1000GP
   SAMPLE_SET=child
fi
DATE=$(date '+%Y%m%d')
FINAL_TABLE=${OUTDIR}/GenDiP/GenDiP_${PANEL}.${PHENO}.${SAMPLE_SET}.Gen3G.european.FW.${DATE}.txt

cat << END > $COMMAND
Rscript scripts/complete_info.R $FILTERED_OUTPUT $PHENO $DATASET $SNP_ANNOTATION $FINAL_TABLE
END

if [ -f $FINAL_TABLE ]; then
   JOBID_complete_info=4
   echo -e "DONE\t$JOB_NAME\t-" >> $JOB_LIST
else
   JOBID_complete_info=$(echo "#! /bin/bash" $COMMAND | sbatch --mail-type=FAIL --mail-user=$JOB_MAIL -A $RAP_ID -D "./" -o ${OUTDIR}/logs/${JOB_NAME}.%j.o -J $JOB_NAME --time=0-4 --mem-per-cpu 20G -c 1 --depend=afterok:$JOBID_filter | grep "[0-9]" | cut -d\  -f4)
   echo -e "$JOBID_complete_info\t$JOB_NAME\t$JOBID_filter" >> $JOB_LIST
fi
#-------------------------------------------------------------------------------
# JOB: 6_result_overview
#-------------------------------------------------------------------------------
# plots

